<?php
$yii=dirname(__FILE__).'/../yii/framework/yii.php';

if($_SERVER['REMOTE_ADDR']=='127.0.0.1') {
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
	$config=dirname(__FILE__).'/protected/config/local.php';
}
else {
	$config=dirname(__FILE__).'/protected/config/main.php';
}

require_once($yii);
Yii::createWebApplication($config)->run();
