<?php $this->pageTitle=Yii::app()->name . ' - Изменение пароля';
$this->breadcrumbs=array(
	'Профиль'=>array('profile'),
	'Изменение пароля'
);
?>
<div class="page-header">
	<h1>Изменение пароля</h1>
</div>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'changepassword-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

<?php echo $form->passwordFieldRow($model,'oldPassword'); ?>
<?php echo $form->passwordFieldRow($model,'password', array('hint'=>'Минимальная длина пароля — 4 символа')); ?>
<?php echo $form->passwordFieldRow($model,'verifyPassword'); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Сохранить')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'label'=>'Отмена', 'url'=>array('profile'))); ?>
</div>

<?php $this->endWidget(); ?>