<?php $this->pageTitle=Yii::app()->name . ' - Профиль';
$this->breadcrumbs=array(
	'Профиль'=>array('profile'),
	'Редактировать информацию',
);
?>
<div class="page-header">
	<h1>
		<?php
		if($profile->company){
			echo $profile->company;
			echo '<br><small>'.$profile->firstname.' '.$profile->lastname.'</small>';
		}
		else
			echo $profile->firstname.' '.$profile->lastname;
		?>
	</h1>
</div>

<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
</div>
<?php endif; ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'profile-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>true,
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary(array($model,$profile)); ?>
<?php echo $form->textFieldRow($model, 'username'); ?>
<?php echo $form->textFieldRow($model, 'email'); ?>
<?php
$profileFields=$profile->getFields();
if ($profileFields) {
	foreach($profileFields as $field) {
		if ($widgetEdit = $field->widgetEdit($profile)) {
			echo $widgetEdit;
		} elseif ($field->range) {
			echo $form->dropDownListRow($profile,$field->varname,Profile::range($field->range));
		} elseif ($field->field_type=="TEXT") {
			echo $form->textAreaRow($profile,$field->varname,array('rows'=>6, 'cols'=>50));
		} else {
			echo $form->textFieldRow($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
		}
		echo $form->error($profile,$field->varname);
	}
}
?>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Сохранить')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'label'=>'Отмена', 'url'=>array('profile'))); ?>
</div>

<?php $this->endWidget(); ?>