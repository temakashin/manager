<?php $this->pageTitle=Yii::app()->name . ' - Профиль';
$this->breadcrumbs=array(
	'Профиль',
);
?>
<div class="page-header">
	<h1>
		<?php
		if($profile->company){
			echo $profile->company;
			echo '<br><small>'.CHtml::encode($profile->firstname).' '.CHtml::encode($profile->lastname).'</small>';
		}
		else
			echo CHtml::encode($profile->firstname).' '.CHtml::encode($profile->lastname);
		?>
	</h1>
</div>
<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
</div>
<?php endif; ?>
<div class="row-fluid">
	<div class="span4">
		<h3><?php echo Yii::app()->user->role=='Client' ? 'Карточка клиента:' : 'Карточка менеджера:';?></h3>
		<dl class="dl-horizontal">
			<dt>Имя Фамилия:</dt>
			<dd><?php echo CHtml::encode($profile->firstname).' '.CHtml::encode($profile->lastname); ?></dd>
			<dt>E-mail:</dt>
			<dd><?php echo CHtml::encode($model->email); ?></dd>
			<dt>Телефон:</dt>
			<dd><?php echo CHtml::encode($profile->phone); ?></dd>
			<dt>Адрес:</dt>
			<dd><?php echo CHtml::encode($profile->address); ?></dd>
		</dl>
		<dl class="dl-horizontal">
			<dt>Логин:</dt>
			<dd><?php echo CHtml::encode($model->username); ?></dd>
			<dt>Пароль:</dt>
			<dd><?php echo CHtml::link('изменить',array('changepassword')); ?></dd>
			<dt>Дата регистрации:</dt>
			<dd><?php echo $model->create_at; ?></dd>
			<dt>Предыдущий визит:</dt>
			<dd><?php echo $model->lastvisit_at; ?></dd>
		</dl>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'Редактировать информацию',
			'size'=>'large', // null, 'large', 'small' or 'mini'
			'type'=>'success',
			'url'=>array('edit'),
		)); ?>
	</div>
	<div class="span4">
		<h3>Документы:</h3>
<?php if(Yii::app()->user->role=='Manager' || Yii::app()->user->role=='Admin'):?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'Новый документ',
			'size'=>'', // null, 'large', 'small' or 'mini'
			'type'=>'primary',
			'url'=>array('/document/create'),
		)); ?>
		<br><br>
		<div class="accordion" id="accordion2">
			<?php $i=1;
			foreach($model->clients as $user):?>
			<?php if($user->user->documents):?>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion<?=$i?>" href="#collapse<?=$i?>">
						<?php echo CHtml::encode($user->user->profile->company);?>
					</a>
				</div>
				<div id="collapse<?=$i;$i++?>" class="accordion-body collapse">
					<div class="accordion-inner">
						<?php
						$total_price=0;
						echo '<dl>';
						foreach($user->user->documents as $document){
							echo '<dt>'.CHtml::link(CHtml::encode($document->title),$document->link);
							if($document->price){
								echo ' '.$document->price.' руб.';
								$total_price += $document->price;
							}
							echo '</dt>';
							echo '<dd>';
							echo $document->description;
							echo '</dd>';
						}
						echo '</dl>';
						if($total_price != 0)
							echo '<p>Итого к оплате в этом месяце: <strong>'.$total_price.' руб.</strong></p>';
						?>
					</div>
				</div>
			</div>
			<?php endif;?>
			<?php endforeach;?>
		</div>
	<?php else:?>
		<?php
		if($model->documents){
			$total_price=0;
			echo '<dl>';
			foreach($model->documents as $document){
				echo '<dt>'.CHtml::link(CHtml::encode($document->title),$document->link);
				if($document->price){
					echo ' '.$document->price.' руб.';
					$total_price += $document->price;
				}
				echo '</dt>';
				echo '<dd>';
				echo $document->description;
				echo '</dd>';
			}
			echo '</dl>';
			if($total_price != 0)
				echo '<p>Итого к оплате в этом месяце: <strong>'.$total_price.' руб.</strong></p>';
		}
		?>
	<?php endif;?>
	</div>
	<div class="span4">
	<?php if(Yii::app()->user->role=='Manager' || Yii::app()->user->role=='Admin'):?>
		<h3>Мои клиенты:</h3>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'Новый клиент',
			'size'=>'', // null, 'large', 'small' or 'mini'
			'type'=>'primary',
			'url'=>array('user/create'),
		)); ?>
		<br><br>
			<ol>
		<?php foreach($model->clients as $user):?>
				<li><?php echo CHtml::link(CHtml::encode($user->user->profile->company),array('/user/user/view', 'id'=>$user->user->id)); ?></li>
			<?php endforeach;?>
			</ol>
	<?php else:?>
		<h3>Персональный менеджер:</h3>
		<?php foreach($model->managers as $manager):?>
		<dl class="dl-horizontal">
			<dt>Имя Фамилия:</dt>
			<dd><?php echo CHtml::encode($manager->manager->profile->firstname).' '.CHtml::encode($manager->manager->profile->lastname); ?></dd>
			<dt>E-mail:</dt>
			<dd><?php echo CHtml::encode($manager->manager->email); ?></dd>
			<dt>Телефон:</dt>
			<dd><?php echo CHtml::encode($manager->manager->profile->phone); ?></dd>
		</dl>
		<?php endforeach;?>
	<?php endif;?>
	</div>
</div>