<?php
$this->breadcrumbs=array(
	'Клиенты'=>array('/user'),
	'Редактировать клиента',
);

?>

<div class="page-header">
	<h1>Редактировать клиента</h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model,'profile'=>$profile)); ?>