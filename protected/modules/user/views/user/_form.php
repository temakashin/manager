<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>true,
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
	'type'=>'horizontal',
));
?>

<?php echo $form->errorSummary(array($model,$profile)); ?>

<?php echo $form->textFieldRow($model,'username',array('maxlength'=>20,'class'=>'span5')); ?>
<?php echo $form->passwordFieldRow($model,'password',array('maxlength'=>128,'class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'email',array('maxlength'=>128,'class'=>'span5')); ?>
<?php echo $form->dropDownListRow($model,'status',User::itemAlias('UserStatus'),array('class'=>'span5')); ?>

<?php
$profileFields=$profile->getFields();
if ($profileFields) {
	foreach($profileFields as $field) {
		if ($widgetEdit = $field->widgetEdit($profile)) {
			echo $widgetEdit;
		} elseif ($field->range) {
			echo $form->dropDownListRow($profile,$field->varname,Profile::range($field->range),array('class'=>'span5'));
		} elseif ($field->field_type=="TEXT") {
			echo $form->textAreaRow($profile,$field->varname,array('rows'=>6,'class'=>'span5'));
		} else {
			echo $form->textFieldRow($profile,$field->varname,array('maxlength'=>(($field->field_size)?$field->field_size:255),'class'=>'span5'));
		}
	}
}
?>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
	'buttonType'=>'submit',
	'type'=>'primary',
	'label'=>$model->isNewRecord ? 'Добавить клиента' : 'Сохранить',
)); ?>
</div>
<?php $this->endWidget(); ?>