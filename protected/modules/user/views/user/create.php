<?php
$this->breadcrumbs=array(
	'Клиенты'=>array('/user'),
	'Новый клиент',
);

?>

<div class="page-header">
	<h1>Новый клиент</h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model,'profile'=>$profile)); ?>