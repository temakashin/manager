<?php $this->pageTitle=Yii::app()->name . ' - Клиент';
$this->breadcrumbs=array(
	'Клиенты'=>array('index'),
	CHtml::encode($model->profile->firstname).' '.CHtml::encode($model->profile->lastname),
);
?>
<div class="page-header">
	<h1>
		<?php
		if($model->profile->company){
			echo $model->profile->company;
			echo '<br><small>'.CHtml::encode($model->profile->firstname).' '.CHtml::encode($model->profile->lastname).'</small>';
		}
		else
			echo CHtml::encode($model->profile->firstname).' '.CHtml::encode($model->profile->lastname);
		?>
	</h1>
</div>

<?php 

// For all users
	$attributes = array(
	);
	
	$profileFields=ProfileField::model()->forAll()->sort()->findAll();
	if ($profileFields) {
		foreach($profileFields as $field) {
			array_push($attributes,array(
					'label' => UserModule::t($field->title),
					'name' => $field->varname,
					'value' => (($field->widgetView($model->profile))?$field->widgetView($model->profile):(($field->range)?Profile::range($field->range,$model->profile->getAttribute($field->varname)):$model->profile->getAttribute($field->varname))),

				));
		}
	}
	array_push($attributes,
		'create_at',
		array(
			'name' => 'lastvisit_at',
			'value' => (($model->lastvisit_at!='0000-00-00 00:00:00')?$model->lastvisit_at:UserModule::t('Not visited')),
		)
	);
			
$this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>$attributes,
)); ?>