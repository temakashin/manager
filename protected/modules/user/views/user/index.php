<?php
$this->breadcrumbs=array(
	'Клиенты',
);
?>
<div class="page-header">
	<h1>Клиенты</h1>
</div>
<?php if(Yii::app()->user->role=='Manager' || Yii::app()->user->role=='Admin'):?>
<?php $this->widget('bootstrap.widgets.TbButton', array(
	'label'=>'Новый клиент',
	'size'=>'large', // null, 'large', 'small' or 'mini'
	'type'=>'success',
	'url'=>array('create'),
)); ?>
<?php endif;?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
	'dataProvider'=>$dataProvider,
	'template'=>"{items}",
	'columns'=>array(
		array(
			'name' => 'username',
			'header'=>'Клиент',
			'type'=>'raw',
			'value' => 'CHtml::link(CHtml::encode($data->profile->firstname)." ".CHtml::encode($data->profile->lastname),array("user/view","id"=>$data->id))',
		),
		array(
			'name'=>'company',
			'header'=>'Компания',
			'value'=>'CHtml::encode($data->profile->company)'
		),
		'create_at',
		'lastvisit_at',
	),
)); ?>
