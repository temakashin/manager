<?php

/**
 * This is the model class for table "document".
 *
 * The followings are the available columns in table 'document':
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $link
 * @property string $description
 * @property integer $price
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class Document extends CActiveRecord
{
	const TYPE_CONTRACT=0;
	const TYPE_BILL=1;

	public function documentType(){
		$type=array(
			self::TYPE_CONTRACT=>'Договор',
			self::TYPE_BILL=>'Счет',
		);
		return $type;
	}

	public function myClients()
	{
		$clients=Manager::model()->findAllByAttributes(array('manager_id'=>Yii::app()->user->id));
		$clientList=array();
		foreach($clients as $client){
			$clientList[$client->user->id] = $client->user->profile->company;
		}
		return $clientList;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Document the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'document';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, title, link', 'required'),
			array('user_id, price, type', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>128),
			array('link', 'length', 'max'=>255),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, title, link, description, price, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'Клиент',
			'title' => 'Название',
			'link' => 'Ссылка на документ',
			'description' => 'Комментарии',
			'price' => 'Стоимость',
			'type' => 'Тип документа'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('type',$this->type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}