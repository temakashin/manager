<?php

/**
 * This is the model class for table "project".
 *
 * The followings are the available columns in table 'project':
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property integer $createtime
 * @property integer $sort
 *
 * The followings are the available model relations:
 * @property Keyword[] $keywords
 */
class Project extends CActiveRecord
{
	const TYPE_SEO = 1;
	const TYPE_CREATE = 2;

	public function projectType()
	{
		if($this->type==self::TYPE_SEO)
			return 'Продвижение сайта';
		elseif($this->type==self::TYPE_CREATE)
			return 'Разработка сайта';
		else
			return '';
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'project';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, url', 'required'),
			array('createtime, sort, type, manager_id, client_id', 'numerical', 'integerOnly'=>true),
			array('title, url', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, url, createtime, sort, type, manager_id, client_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'keywords' => array(self::HAS_MANY, 'Keyword', 'project_id'),
			'client' => array(self::BELONGS_TO, 'User', 'client_id'),
			'manager' => array(self::BELONGS_TO, 'User', 'manager_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'url' => 'http://',
			'createtime' => 'Createtime',
			'sort' => 'Сортировка',
			'type' => 'Тип проекта'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('createtime',$this->createtime);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('type',$this->type);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('manager_id',$this->manager_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeSave()
	{
		if(parent::beforeSave()) {
			if($this->isNewRecord) {
				$this->createtime=time();
				$this->type=self::TYPE_SEO;
			}
			return true;
		}
		else
			return false;
	}
}