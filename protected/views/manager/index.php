<?php
$this->breadcrumbs=array(
	'Managers',
);

$this->menu=array(
	array('label'=>'Create Manager','url'=>array('create')),
	array('label'=>'Manage Manager','url'=>array('admin')),
);
?>

<h1>Managers</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
