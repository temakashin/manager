<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Вход';
$this->breadcrumbs=array(
	'Вход',
);
?>
<div class="page-header">
	<h1>Вход</h1>
</div>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
	'htmlOptions'=>array('class'=>'well'),
)); ?>

<?php echo $form->textFieldRow($model,'username'); ?>
<?php echo $form->passwordFieldRow($model,'password'); ?>
<?php echo $form->checkboxRow($model,'rememberMe'); ?>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Войти')); ?>

<?php $this->endWidget(); ?>