<?php
/* @var $this KeywordController */
/* @var $model Keyword */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'keyword-form',
	'type'=>'horizontal',
)); ?>

<fieldset>
<?php echo $form->textFieldRow($model,'keyword'); ?>
<?php echo $form->textFieldRow($model,'sort'); ?>
</fieldset>
<div class="form-actions">
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>$model->isNewRecord ? 'Добавить' : 'Сохранить')); ?>
</div>

<?php $this->endWidget(); ?>