<?php
/* @var $this KeywordController */
/* @var $model Keyword */

$this->breadcrumbs=array(
	'Проекты'=>array('project/index'),
	$model->projects[$model->project_id]=>array('project/view','id'=>$model->project_id),
	'Новое ключевое слово',
);
?>
<div class="page-header">
	<h1>
		<?php echo $model->projects[$model->project_id];?><br>
		<small>Новое ключевое слово</small>
	</h1>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>