<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Проекты'=>array('index'),
	'Добавить проект',
);
?>
<div class="page-header">
	<h1>
		Проекты<br>
		<small>Добавить проект</small>
	</h1>
</div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>