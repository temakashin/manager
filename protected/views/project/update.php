<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Проекты'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Редактировать проект',
);
?>
<div class="page-header">
	<h1>
		<?php echo $model->title;?><br>
		<small>Редактировать проект</small>
	</h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>