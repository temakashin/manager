<?php
/* @var $this ProjectController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Проекты',
);
?>
<div class="page-header">
	<h1>Проекты</h1>
</div>
<?php if(Yii::app()->user->role!='Client'):?>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Добавить проект',
    'type'=>'success', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'large', // null, 'large', 'small' or 'mini'
    'url'=>array('create')
));
	?>
<?php endif;?>

<?php
if(Yii::app()->user->role!='Client')
	$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$dataProvider,
    'template'=>"{items}",
    'columns'=>array(
        array('name'=>'title', 'header'=>'Название', 'type'=>'raw', 'value'=>'CHtml::link(CHtml::encode($data->title), array("view", "id"=>$data->id))'),
        array('name'=>'url', 'header'=>'Адрес сайта', 'value'=>'$data->url'),
	    array('name'=>'createtime', 'header'=>'Дата добавления', 'value'=>'date("d.m.Y",$data->createtime)'),
	    array('name'=>'type', 'header'=>'Тип проекта', 'value'=>'$data->projectType()'),
	    array('name'=>'client_id', 'header'=>'Клиент', 'type'=>'raw', 'value'=>'CHtml::link(CHtml::encode($data->client->profile->company), array("/user/user/view", "id"=>$data->client->id))'),
    ),
));
else
	$this->widget('bootstrap.widgets.TbGridView', array(
		'type'=>'striped bordered condensed',
		'dataProvider'=>$dataProvider,
		'template'=>"{items}",
		'columns'=>array(
			array('name'=>'title', 'header'=>'Название', 'type'=>'raw', 'value'=>'CHtml::link(CHtml::encode($data->title), array("view", "id"=>$data->id))'),
			array('name'=>'url', 'header'=>'Адрес сайта', 'value'=>'$data->url'),
			array('name'=>'createtime', 'header'=>'Дата добавления', 'value'=>'date("d.m.Y",$data->createtime)'),
			array('name'=>'type', 'header'=>'Тип проекта', 'value'=>'$data->projectType()'),
		),
	));
?>
