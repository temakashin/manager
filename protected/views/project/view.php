<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Проекты'=>array('index'),
	$model->title,
);
?>
<div class="page-header">
	<h1>
		<?php echo $model->title; ?><br>
		<small><?php echo $model->url; ?></small>
	</h1>
</div>
<?php
$currentDate = date('F',mktime(0,0,0,$month));

$nextMonth = $month+1;
$prevMonth = $month-1;

if($nextMonth <= 12)
	$nextMonth = $nextMonth++;
else {
	$nextMonth = 1;
	$year++;
	}

if($prevMonth >= 1)
	$prevMonth = $prevMonth--;
else {
	$prevMonth = 12;
	$year--;
	}
?>
<?php if(Yii::app()->user->role!='Client'):?>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Добавить ключевое слово',
    'type'=>'success', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'large', // null, 'large', 'small' or 'mini'
    'url'=>array('keyword/create','project'=>$model->id),
)); ?> 

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Редактировать проект',
    'size'=>'large', // null, 'large', 'small' or 'mini'
    'url'=>array('update','id'=>$model->id),
)); ?>
<?php endif;?>
<br><br>
<table class="table table-striped table-bordered table-condensed positions" data-graph-container-before="1" data-graph-type="column">
	<thead>
		<tr>
			<th><?php echo $currentDate.' '.$year;?> <?php echo CHtml::link(CHtml::encode('←'),array('view','id'=>$model->id,'month'=>$prevMonth,'year'=>$year)); ?> <?php echo CHtml::link(CHtml::encode('→'),array('view','id'=>$model->id,'month'=>$nextMonth,'year'=>$year)); ?></th>
<?php
for($i = $days; $i > 0; $i--) {
	echo '<th class="day">'.$i.'</th>';
}
?>
		</tr>
	</thead>
	<tbody>
<?php
foreach ($keywords as $keyword) {
	echo '<tr>';
	echo '<td class="keyword">'.$keyword->keyword.'</td>';

	for($i = $days; $i > 0; $i--) {
		$done=false;
		$positions=Position::model()->findAllByAttributes(array('keyword_id'=>$keyword->id),array('order'=>'position DESC'));
		if($positions){
			foreach ($positions as $position) {
				if($i == date('d',$position->date) && $month==date('m',$position->date) && $year==date('Y',$position->date)){
					$class='';
					if($position->position<=10)
						$class = ' class="success"';
					if($position->position!=100)
						echo '<td'.$class.'>'.$position->position.'</td>';
					else
						echo '<td>—</td>';
					$done=true;
					break;
				}
			}
			if(!$done){
				echo '<td></td>';
				$done=false;
			}
		}
	}

	echo '</tr>';
}
?>
	</tbody>
</table>

<? /*
<div id="chart_div" style="width: 100%; height: 500px;"></div>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
<?php

echo "['',";
foreach ($keywords as $keyword)
	echo "'".$keyword->keyword."',"; //слова, слова, слова
echo "],\n";


for($i = 1; $i <= $days; $i++) {
	echo "['".$i."',";

	foreach ($keywords as $keyword) {
		$position=Position::model()->findAllByAttributes(array('keyword_id'=>$keyword->id));
		foreach ($positions as $position) {
			if($i == date('d',$position->date) && $month==date('m',$position->date) && $year==date('Y',$position->date)){
				echo $position->position.",";
				break;
			}
		}
	}

	echo "],\n";
}

?>
        ]);

        var options = {
          chartArea: {left:38,top:30, width:"80%",height:"70%"},
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>

*/

