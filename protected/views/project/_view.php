<?php
/* @var $this ProjectController */
/* @var $data Project */
?>

<div class="view">
	<h2><?php echo CHtml::link(CHtml::encode($data->title), array('view', 'id'=>$data->id)); ?></h2>
	<b><?php echo CHtml::encode($data->url); ?></b>
	<br />
	<?php echo CHtml::encode(date('d.m.Y',$data->createtime)); ?>
</div>