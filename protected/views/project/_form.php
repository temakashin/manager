<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'project-form',
	'type'=>'horizontal',
)); ?>
<fieldset>
<?php echo $form->textFieldRow($model,'title'); ?>
<?php echo $form->textFieldRow($model,'url'); ?>
<?php echo $form->textFieldRow($model,'sort'); ?>
</fieldset>
<div class="form-actions">
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>$model->isNewRecord ? 'Добавить' : 'Сохранить')); ?>
</div>

<?php $this->endWidget(); ?>
