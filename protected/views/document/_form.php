<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'document-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->dropDownListRow($model,'user_id',$model->myClients(),array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>128,'hint'=>'Формат: 1209124. Договор. Продвижение. Приложение 1')); ?>
<?php echo $form->dropDownListRow($model,'type',$model->documentType(),array('class'=>'span5')); ?>
<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>255, 'hint'=>'Формат: https://docs.google.com/document/d/1IXIYpKnp9x3tFb0_0nGEeCMgHpS1zOV5o9zciDvfjR0/edit')); ?>
<?php echo $form->textFieldRow($model,'price',array('class'=>'span5')); ?>
<?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span5')); ?>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Добавить' : 'Сохранить',
	)); ?>
</div>
<?php $this->endWidget(); ?>
