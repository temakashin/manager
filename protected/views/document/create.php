<?php
$this->breadcrumbs=array(
	'Профиль'=>array('/user/profile'),
	'Новый документ',
);

?>

<div class="page-header">
	<h1>Новый документ</h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>