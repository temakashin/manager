<?php
$this->breadcrumbs=array(
	'Профиль'=>array('/user/profile'),
	'Редактировать документ',
);

?>

<div class="page-header">
	<h1>Редактировать документ</h1>
</div>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>