<?php
/* @var $this SearchengineController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Searchengines',
);

$this->menu=array(
	array('label'=>'Create Searchengine', 'url'=>array('create')),
	array('label'=>'Manage Searchengine', 'url'=>array('admin')),
);
?>

<h1>Searchengines</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
