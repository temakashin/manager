<?php
/* @var $this SearchengineController */
/* @var $model Searchengine */

$this->breadcrumbs=array(
	'Searchengines'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Searchengine', 'url'=>array('index')),
	array('label'=>'Create Searchengine', 'url'=>array('create')),
	array('label'=>'View Searchengine', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Searchengine', 'url'=>array('admin')),
);
?>

<h1>Update Searchengine <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>