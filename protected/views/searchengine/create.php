<?php
/* @var $this SearchengineController */
/* @var $model Searchengine */

$this->breadcrumbs=array(
	'Searchengines'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Searchengine', 'url'=>array('index')),
	array('label'=>'Manage Searchengine', 'url'=>array('admin')),
);
?>

<h1>Create Searchengine</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>