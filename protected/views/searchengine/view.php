<?php
/* @var $this SearchengineController */
/* @var $model Searchengine */

$this->breadcrumbs=array(
	'Searchengines'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Searchengine', 'url'=>array('index')),
	array('label'=>'Create Searchengine', 'url'=>array('create')),
	array('label'=>'Update Searchengine', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Searchengine', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Searchengine', 'url'=>array('admin')),
);
?>

<h1>View Searchengine #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'url',
	),
)); ?>
