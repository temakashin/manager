<?php /* @var $this Controller */ ?>
<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/styles.css'); ?>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>

<body id="top">
<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'type'=>'inverse',
	'brand'=>CHtml::encode(Yii::app()->name),
	'collapse'=>true,
    'fluid'=>true,
	'items'=>array(
		array(
			'class'=>'bootstrap.widgets.TbMenu',
			'items'=>array(
				array('label'=>'Главная', 'url'=>array('/site/index')),
				array('label'=>'Проекты', 'url'=>array('/project/'), 'visible'=>!Yii::app()->user->isGuest, 'active'=>Yii::app()->controller->id=='project' || Yii::app()->controller->id=='keyword'),
			),
		),
		array(
			'class'=>'bootstrap.widgets.TbMenu',
			'htmlOptions'=>array('class'=>'pull-right'),
			'items'=>array(
				array('label'=>'Профиль', 'icon'=>'user', 'url'=>array('/user/profile/'), 'visible'=>!Yii::app()->user->isGuest, 'active'=>Yii::app()->controller->id=='profile'),
				array('label'=>'Вход', 'url'=>array('/user/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Выход ('.Yii::app()->user->name.')', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest),
			),
		),
	),
)); ?>

	<div class="container-fluid">
	<?php if(isset($this->breadcrumbs)):?>
	<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'homeLink'=>CHtml::link('Главная', Yii::app()->homeUrl),
	)); ?>
	<?php endif?>

	<div class="row-fluid">
	<?php echo $content; ?>
	</div>

	<hr />
	<footer>
		<p>&copy; 2012 <?php echo CHtml::encode(Yii::app()->name); ?></p>
	</footer>

</div>

</body>
</html>