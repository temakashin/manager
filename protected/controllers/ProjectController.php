<?php

class ProjectController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}
	public function allowedActions() {
		//return 'admin,delete,index,view,create,update,updatePosition,yandexPositionXML,googlePosition';
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	public function actionView($id,$month=null,$year=null)
	{
		if(!$month)
			$month=date('m',time());
		if(!$year)
			$year=date('Y',time());
		
		$days = cal_days_in_month(CAL_GREGORIAN, $month, $year);

		$keywords=Keyword::model()->findAllByAttributes(array('project_id'=>$id));

		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'keywords'=>$keywords,
			'days'=>$days,
			'month'=>$month,
			'year'=>$year,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Project;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$model->attributes=$_POST['Project'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$model->attributes=$_POST['Project'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

		if(Yii::app()->user->role=='Manager' || Yii::app()->user->role=='Admin')
			$condition='manager_id='.Yii::app()->user->id;
		else
			$condition='client_id='.Yii::app()->user->id;
		$dataProvider=new CActiveDataProvider('Project', array(
			'criteria'=>array(
				'condition'=>$condition,
			))
		);

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Project('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Project']))
			$model->attributes=$_GET['Project'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		//$model=Project::model()->findByPk($id);
		if(Yii::app()->user->role=='Manager' || Yii::app()->user->role=='Admin')
			$model=Project::model()->findByAttributes(array('id'=>$id,'manager_id'=>Yii::app()->user->id));
		else
			$model=Project::model()->findByAttributes(array('id'=>$id,'client_id'=>Yii::app()->user->id));

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='project-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionUpdatePosition($id=null)
	{
		$g=new YandexParser();
		$yandex = Searchengine::model()->findByAttributes(array('title'=>'Yandex'));
		$url = 'noevkovcheg76.ru';

		if($res=$g->analyzeThis('noevkovcheg76.ru', 'детские торты в ярославле'))
			print_r($res);

		if($id){
	








			// $project=Project::model()->findbyPk($id);
			// //$keywords=Keyword::model()->findAllByAttributes(array('project_id'=>$project->id));
			// $keywords=Keyword::model()->findAllByAttributes(array('id'=>$project->id));

			// foreach($keywords as $keyword){
			// 	if($res=$g->analyzeThis($url, $keyword->keyword)){
			// 		echo '<span style=«color: green;»>'.$res[0].'-я позиция сайта '.$url.' по фразе <a href="'.$url.'"'.$keyword->keyword.'</a>"</span>';
			// 	}
			// 	else
			// 		echo $url.' не найден в первых '.$g->resultsLimit.' результатах по фразе "'.$keyword->keyword.'"';
			// 	sleep(rand(3, 5));
			// }
			// foreach($keywords as $keyword){
			// 	$positions=Position::model()->findAllByAttributes(array('keyword_id'=>$keyword->id,'searchengine_id'=>$yandex->id));
			// 	if($positions)
			// 		foreach ($positions as $position) {
			// 			$datePosition=date('m.Y',$position->date);
			// 			$dateNow=date('m.Y',time());
			// 			if($datePosition==$dateNow)
			// 				$position->delete();
			// 			$this->getPlaceYandex($project->url, $keyword);
			// 		}
			// 	else
			// 		$this->getPlaceYandex($project->url, $keyword);
			// }
			$this->render('test',array(

				));
		}
	}

	public function actionYandexPositionXML($id=null)
	{
//		$yandex = Searchengine::model()->findByAttributes(array('title'=>'Yandex'));

		if($id){
			$project=Project::model()->findbyPk($id);
			$keywords=Keyword::model()->findAllByAttributes(array('project_id'=>$project->id));
			foreach($keywords as $keyword){
//				$positions=Position::model()->findAllByAttributes(array('keyword_id'=>$keyword->id,'searchengine_id'=>$yandex->id));
//				if($positions)
//					foreach ($positions as $position) {
//						$datePosition=date('m.Y',$position->date);
//						$dateNow=date('m.Y',time());
//						if($datePosition==$dateNow)
//							$position->delete();
//						$this->getPlaceYandex($project->url, $keyword);
//					}
//				else
					$this->getPlaceYandex($project->url, $keyword);
			}
			$this->redirect(array('view','id'=>$id));
		}
		else
		{
			$projects=Project::model()->findAll();

			foreach ($projects as $project) {
				$keywords=Keyword::model()->findAllByAttributes(array('project_id'=>$project->id));
				foreach($keywords as $keyword){
//					$positions=Position::model()->findAllByAttributes(array('keyword_id'=>$keyword->id,'searchengine_id'=>$yandex->id));
//					if($positions)
//						foreach ($positions as $position) {
//							$datePosition=date('m.Y',$position->date);
//							$dateNow=date('m.Y',time());
//							$this->getPlaceYandex($project->url, $keyword);
//							if($datePosition==$dateNow)
//								$position->delete();
//						}
//					else
						$this->getPlaceYandex($project->url, $keyword);
				}
			}
			$this->redirect(array('index'));
		}
	}

	public function getPlaceYandex($host, $query)
	{
		$yandex = Searchengine::model()->findByAttributes(array('title'=>'Yandex'));
		$host = preg_replace("[^http://|www\.]", '', $host);
		$query_esc = htmlspecialchars($query->keyword);
		$host_esc  = htmlspecialchars($host);
		$page  = 0;
		$found = 0;
		$pages = 10;
		$error = false;
		$exit = false;

		while (!$exit && $page < $pages && $host) {
			$doc = <<<DOC
<?xml version='1.0' encoding='utf-8'?>
<request>
    <query>$query_esc</query>
    <page>$page</page>
    <maxpassages>0</maxpassages>
    <groupings>
        <groupby attr='d' mode='deep' groups-on-page='10' docs-in-group='1' curcateg='-1'/>
    </groupings>
</request>
DOC;
			$context = stream_context_create(array(
				'http' => array(
					'method'=>"POST",
					'header'=>"Content-type: application/xml\r\n"."Content-length: ".strlen($doc),
					'content'=>$doc
					)
				));
			$response = file_get_contents($yandex->url, true, $context);
			if ($response) {
				$xmldoc = new SimpleXMLElement($response);
				$xmlresponce = $xmldoc->response;
				if ($xmlresponce->error) {
					echo "Возникла следующая ошибка: " . $xmlresponce->error . "<br/>\n";
					$error = true;
					break;
				}
				$pos = 1;
				$nodes = $xmldoc->xpath('/yandexsearch/response/results/grouping/group/doc/url');
				foreach ($nodes as $node) {
					// если URL начинается с имени хоста, выходим из цикла
					if ( preg_match('/^http:\/\/(www\.)?'.$host.'/ui', $node) ) {
						$found = $pos + $page * 10;
						$exit = 1;
						break;
					}
					$pos++;
				}
				$page++;
			} else {
				echo "внутренняя ошибка сервера\n";
				$exit = 1;
			}
		}

		if (!$error) {
			if ($found) {
				$position=new Position;
				$position->keyword_id=$query->id;
				$position->searchengine_id=$yandex->id;
				$position->date=time();
				$position->position=$found;
				$position->save();
			}
			else {
				$position=new Position;
				$position->keyword_id=$query->id;
				$position->searchengine_id=$yandex->id;
				$position->date=time();
				$position->position='100';
				$position->save();
			} 
		}
	}

	public function actionGooglePosition($id)
	{
		if($id){
			$project=Project::model()->findbyPk($id);
			$keywords=Keyword::model()->findAllByAttributes(array('project_id'=>$project->id));

			foreach($keywords as $keyword){
				echo '<small><i>' . $keyword->keyword.'...</i></small><br>';
				$lk = $this->getGoogleLinks(trim($keyword->keyword));
				foreach($lk as $n=>$url)
					if($this->IsMyDomen($url, array($project->url))) {
						echo '<b>Фраза: </b>' . $keyword->keyword . ' <b>Место: </b>' ;
						echo $this->colorate($n+1) ;
						@flush();
					}
			}
			$this->render('test',array(

			));
		}
	}

	public function getGoogleLinks($keyword)
	{
		$countPage = 100;
		$pageNum = 1;
		$url = 'https://www.google.ru/search?q=' . urlencode( $keyword) . '&num='.$countPage.'&hl=ru&start=' . $pageNum . '&ie=UTF-8';
		$page = file_get_contents($url);
		if(!$page)
			$page = $this->curlgoogle($url);

		if(!$page) {
			echo 'Page dont downloaded<br>';
			return array();
		}
		else {
			if(preg_match_all('/<h3 class="r"><a href="(.+?)"/is', $page, $match))
				return $match['1'];
			else
				print('По запросу "'.$keyword.'" линков в гугле нет ?<br>');
			return array();
		}
	}

	public function IsMyDomen($url, $Array) {
		$U1 = explode('/', $url);
		foreach($Array as $url2)
		{
			$U2 = explode('/', $url2);
			if($U1 == $U2)
				return true;
		}
		return false;
	}

	public function colorate($int)
	{
		$color = '#FF0000';  //красный
		if($int<=10)
			$color = '#008E00'; //зеленый
		if($int>10  && $int<=20)
			$color = '#FFE500'; //желтый
		return '<span style="color: ' . $color . '">' . $int . '</span><br />';
	}

	public function curlgoogle($url)
	{
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_URL,$url);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_FOLLOWLOCATION,true);
		curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,300);
		return curl_exec($curl);
	}


}
