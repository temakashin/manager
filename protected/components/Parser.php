<?php
abstract class Parser
{
	public abstract function analyzeThis($url);

	// получение имени хоста из url (parse_url с дополнительным функционалом, поскольку убедился что просто parse_url не всегда почему-то работает, когда url слишком неудобочитаемый)
	public function getHost($url){
		$url=@parse_url($url);
		if($url['path'] && !$url['host'])
			$url['host']=$url['path'];
		$url['host']=preg_replace("/.*$", "", $url['host']);
		$url['host']=preg_replace("^www\.", "", $url['host']);

		return $url['host'];
	}

	// функция сравнения 2-х url на предмет принадлежности к одному хосту
	protected function compareURL($url1, $url2){
		$url1=$this->getHost($url1);
		$url2=$this->getHost($url2);

		return(strtoupper($url1['host'])==strtoupper($url2['host']) ? true : false);
	}
}