<?php
class YandexParser extends Parser
{
	public $resultsLimit=50; // лимит результатов выдачи
	public $url;
	public $keyword;
	public $region=16;

	public $resultsOnPage=50; // можно только 10, 20, 30, 50

	public function analyzeThis($url, $keyword=''){
		$this->url=$url;
		$this->keyword=$keyword;
		$x=0;
		while($x*$this->resultsOnPage<=$this->resultsLimit-1){
			if($results=$this->analyzePage(str_replace(array("\r", "\n", "\t"), '', $this->downloadPage($x)))){
				$results[0]=$x*$this->resultsOnPage+$results[0];
				return $results;
			}
			$x++;
			sleep(rand(3, 5));
		}
		return false;
	}

	protected $regexpParseResults='#<li class="b-serp-item">.*<a[^>]*b-serp-url__link[^>]*href="([^<>"]+)"[^>]*>(.+)</a>.*</li>#Ui';
	protected $captcha='#<div class="b-captcha">.*<img src="(.+)"[^>]*b-captcha__image[^>]*>.*</div>#Ui';
	protected $urlMask='http://yandex.ru/yandsearch?text=[KEYWORD]&p=[PAGE_NUMBER]&numdoc=[RESULTS_ON_PAGE]&lr=[REGION]';

	protected function downloadPage($pageNumber){
		$mask=str_replace('[KEYWORD]', urlencode($this->keyword), $this->urlMask);
		$mask=str_replace('[PAGE_NUMBER]', $pageNumber, $mask);
		$mask=str_replace('[RESULTS_ON_PAGE]', $this->resultsOnPage, $mask);
		$mask=str_replace('[REGION]', $this->region, $mask);

		return file_get_contents($mask);
	}

	protected function analyzePage($content){
		if(preg_match_all($this->regexpParseResults, $content, $matches, PREG_SET_ORDER)!==false){
			if(count($matches)<=0){
				if(!$this->getCaptcha($this->captcha,$content))
					echo '<br /><span style=«color: red;»>Не найдено вхождений или ошибка парсера: возможно гугл подозревает, что Вы робот!</span>';
				else {
					$ch='';
					curl_setopt($ch, CURLOPT_URL, $this->getCaptcha($this->captcha,$content));
					// установка метода передачи параметров
					curl_setopt($ch, CURLOPT_POST, 0);
					// установка браузера
					curl_setopt($ch, CURLOPT_USERAGENT, "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13");
					// добавляем заголовков к нашему запросу.
					$headers = array(
						'Accept: image/png,image/*;q=0.8,*/*;q=0.5',
						'Accept-Language: ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3',
						'Accept-Encoding: deflate',
						'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7'
					);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				}
			}
			else
				foreach($matches as $num=>$match){
					if($this->compareURL($this->getUrlFromYa($match[1]), $this->url))
						return array($num+1, $match[1], $match[2]);
				}
		}
		else
			echo '<span style=«color: red;»>Не найдено вхождений или ошибка парсера: возможно йандекс подозревает, что Вы робот!</span>';

		return false;
	}

	protected function getUrlFromYa($yaUrl){
		if(preg_match_all('/[\*](http:.*)$/Ui', $yaUrl, $matches, PREG_SET_ORDER)!==false)
			return $matches[0][1];
		return false;
	}

	protected function getCaptcha($captcha,$content){
		if(preg_match_all($captcha, $content, $matches, PREG_SET_ORDER)!==false)
			return $matches[0][1];
		return false;
	}
}